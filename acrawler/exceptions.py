#!/usr/bin/env python
# encoding=utf-8


class FieldException(Exception):
    """General Exception"""
    pass


class TextException(FieldException):
    """TextField extract_value error"""
    pass
