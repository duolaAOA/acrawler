#!/usr/bin/env python
# encoding=utf-8

from lxml.etree import _Element, _ElementUnicodeResult


class BaseField:
    """
    BaseField   class
    """
    def __init__(self, css_select=None, xpath_select=None, default=None):
        """
        :param css_select:      Css  Selector
        :param xpath_select:    Xpath  Selector
        """
        self.css_select = css_select
        self.xpath_select = xpath_select
        self.default = default


class TextField(BaseField):
    """
    TextField   Class
    """
    def __init__(self, css_select=None, xpath_select=None, default=None):
        super().__init__(css_select, xpath_select, default)

    def extract_value(self, html: _Element) -> list:
        """
        Use css_select or re_select to extract a field value
        :param html:    html  response
        :return:
        """
        value = ''
        if self.css_select:
            value = html.cssselect(self.css_select)

        elif self.xpath_select:
            value = html.xpath(self.xpath_select)
        else:
            raise ValueError('%s field: css_select or xpath_select is expected'
                                 % self.__class__.__name__)
        if isinstance(value, list) and len(value) == 1:
            if isinstance(value[0], _Element):
                text = ''
                for node in value[0].itertext():
                    text += node.strip()
                value = text
            if isinstance(value[0], str) or isinstance(value[0], _ElementUnicodeResult):
                value = ''.join(value)
        if self.default is not None:
            value = value if value else self.default
        return value


class AttrField(BaseField):
    """
    AttrField class
    """
    def __init__(self, attr, css_select=None, xpath_select=None, default=None):
        super().__init__(css_select, xpath_select, default)
        self.attr = attr

    def extract_value(self, html, is_source=False) -> list:
        """
        Use css_select or re_select to extract a field value
        :param html:    html  response
        :return:
        """
        if self.css_select:
            value = html.cssselect(self.css_select)
            value = value[0].get(self.attr, value).strip() if len(value) == 1 else value
        elif self.xpath_select:
            value = html.xpath(self.xpath_select)
        else:
            raise ValueError('%s field: css_select or xpath_select is expected'
                                % self.__class__.__name__)
        if is_source:
            return value
        if self.default is not None:
            value = value if value else self.default
        return value


