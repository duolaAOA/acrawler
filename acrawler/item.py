#!/usr/bin/env python
# encoding=utf-8

import requests

from lxml import etree
import cchardet
from lxml.etree import _Element
from acrawler.field import BaseField
from .utils import get_random_user_agent


def with_metaclass(meta):
    return meta("acrawler", (object,), {})


class ItemMeta(type):
    """
    MetaClass for item
    """
    def __new__(cls, name: str, bases: tuple, attrs: dict):
        _fields = {}
        for field_name, object in attrs.items():
            if isinstance(object, BaseField):
                _fields[field_name] = object
        for field_name in _fields.keys():
            attrs.pop(field_name)
        attrs['_fields'] = _fields
        return type.__new__(cls, name, bases, attrs)


class Item(with_metaclass(ItemMeta)):
    """
    Item Class
    """
    def __init__(self, html: _Element) -> None:
        if html is None or not isinstance(html, etree._Element):
            raise ValueError("etree._Element is expected")

        for field_name, field_value in self._fields.items():

            get_field = getattr(self, 'tal_{}'.format(field_name), None)
            value = field_value.extract_value(html) if isinstance(field_value, BaseField) else field_value
            if get_field:
                value = get_field(value)
            setattr(self, field_name, value)

    @classmethod
    def _get_html(cls, html: _Element, url, html_etree, params, **kwargs) -> _Element:
        if html:
            html = etree.HTML(html)
        elif url:
            if not kwargs.get("headers", None):
                kwargs["headers"] = {
                    "User-Agent": get_random_user_agent()
                }
            response = requests.get(url, params, **kwargs)
            response.raise_for_status()
            content = response.content
            charset = cchardet.detect(content)
            text = content.decode(charset["encoding"])
            html = etree.HTML(text)
        elif html_etree is not None:
            return html_etree
        else:
            raise ValueError("html (url or html_etree) is expected")
        return html

    @classmethod
    def get_item(cls, html='', url='', html_etree=None, params=None, **kwargs):
        html = cls._get_html(html, url, html_etree, params=params, **kwargs)
        item = {}
        ins_item = cls(html=html)
        for i in cls._fields.keys():
            item[i] = getattr(ins_item, i)
        return item

    @classmethod
    def get_items(cls, html='', url='', html_etree=None, params=None, **kwargs):
        html = cls._get_html(html, url, html_etree, params=params, **kwargs)
        items_field = cls._fields.get('target_item', None)
        if items_field:
            items = items_field.extract_value(html)
            return [cls(html=i) for i in items]
        else:
            raise ValueError("target_item is expected")