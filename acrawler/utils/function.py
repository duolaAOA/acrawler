#!/usr/bin/env python
# encoding=utf-8

import os
import time
import random
from urllib.parse import urlparse
from datetime import datetime


USER_AGENT_FILE_PATH = './user_agents.txt'


class Delay():
    """
    set delay time
    """
    def __init__(self):
        self.domains = {}

    def sleep(self, url, delay_time):
        domain = get_domain(url)
        last_accessed = self.domains.get(domain)
        if delay_time > 0 and last_accessed is not None:
            sleep_seconds = delay_time - (datetime.now() - last_accessed).seconds

            if sleep_seconds > 0:
                time.sleep(sleep_seconds)
        self.domains[domain] = datetime.now()
        print(self.domains)


def get_random_user_agent():
    """
    get a random user agent
    :return:   random user agent
    """
    USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 ' \
                 '(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'
    return random.choice(_get_user_agents(USER_AGENT_FILE_PATH, USER_AGENT))


def _get_user_agents(filename, default=''):
    """
    get user_agent from file
    :param filename:
    :param default:
    :return:  data
    """
    root_folder = os.path.dirname(__file__)
    user_agents_file = os.path.join(root_folder, filename)
    try:
        with open(user_agents_file) as f:
            data = [_.strip() for _ in f.readlines()]
    except:
        data = [default]
    return data


def get_domain(url):
    domain = urlparse(url).netloc
    return domain
