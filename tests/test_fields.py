#!/usr/bin/env python
# encoding=utf-8

import unittest

from lxml import etree

from acrawler import TextField, AttrField


html = """
<html>
    <head>
        <title>TEST</title>
    </head>
    <body>
        <p>
            <a class="test_link" href="https://test123.com">hello world.</a>
        </p>
    </body>
</html>

"""


class TestField(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.html = etree.HTML(html)

    def test_css_select(self):
        field = TextField(css_select="head title")
        value = field.extract_value(self.html)
        self.assertEqual(value, "TEST")

    def test_xpath_select(self):
        field = TextField(xpath_select="/html/head/title")
        value = field.extract_value(self.html)
        self.assertEqual(value, "TEST")

    def test_attr_field(self):
        attr_field = AttrField(css_select="p a.test_link", attr="href")
        value = attr_field.extract_value(self.html)
        self.assertEqual(value, "https://test123.com")


if __name__ == '__main__':
    unittest.main()
